<link rel="stylesheet" href="style.css">
<?php
require 'molla.php';

$formSubmitted = false;

// Define PATH constant if not already defined
if (isset($_POST['submit'])) { 

    $score = $_POST['score'];
    $name = $_POST['name'];
    $screenshot = $_FILES['screenshot']['name'];

    // Check if all required fields are filled
    if (!empty($name) && is_numeric($score) && !empty($screenshot)) {
        $screenshot_type = $_FILES['screenshot']['type'];
        $screenshot_size = $_FILES['screenshot']['size'];

        // Check if file type and size are valid
        if ((($screenshot_type == 'image/gif') || ($screenshot_type == 'image/jpeg') || ($screenshot_type == 'image/pjpeg') || ($screenshot_type == 'image/png')) && ($screenshot_size > 0) && ($screenshot_size <= GW_MAXFILESIZE)) {
            if ($_FILES['screenshot']['error'] == 0) {
                // Move the file to the target upload folder
                $target = PATH . $screenshot;
                if (move_uploaded_file($_FILES['screenshot']['tmp_name'], $target)) { 
                    // Connect to the database
                    $dbc = mysqli_connect('localhost', 'root', '', 'elvis_store');

                    // Write the data to the database (use prepared statement to prevent SQL injection)
                    $query = "INSERT INTO guitarwars (name, score, screenshot, approved) VALUES ('$name', '$score', '$screenshot', 0)";
                    mysqli_query($dbc, $query);
                    
                    // Confirm success with the user
                    echo '<p>Thanks for adding your new high score!</p>';
                    echo '<p><strong>Name:</strong> ' . $name . '<br />';
                    echo '<strong>Score:</strong> ' . $score . '<br />';
                    echo '<img src="' . PATH . $screenshot . '" alt="Score image" /></p>';
                    echo '<p><a href="index.php">&lt;&lt; Back to high scores</a></p>';

                    // Clear the score data to clear the form
                    $name = "";
                    $score = "";
                    $screenshot = "";

                    // Set formSubmitted flag to true
                    $formSubmitted = true;
                } else {
                    echo '<p class="error">Sorry, there was a problem uploading your screenshot image.</p>'; 
                }
            } else {
                echo '<p class="error">Error uploading file.</p>';
            }
        } else {
            echo '<p class="error">The screenshot must be a GIF, JPEG, or PNG image file no greater than ' . (GW_MAXFILESIZE / 1024) . ' KB in size.</p>';
            @unlink($_FILES['screenshot']['tmp_name']); // Try to delete the temporary screenshot image file
        }
    } else {
        echo '<p class="error">Please enter all of the information to add your high score.</p>';
    }
}
?>

<?php if (!$formSubmitted): ?>
    <form enctype="multipart/form-data" method="post" action="<?php echo $_SERVER['PHP_SELF']; ?>">
    <label for="name">Name:</label>
    <input type="text" name="name" value="<?php if (!empty($name)) echo $name; ?>"><br>
    <label for="name">Score:</label>
    <input type="text" name="score" value="<?php if (!empty($score)) echo $score; ?>"><br>
    <?php if (!empty($score) && !is_numeric($score)): ?>
    <p style="color: red;">The value in the score field must be numeric.</p>
    <?php endif; ?>
    <label for="screenshot">Label for Screenshot:</label>
    <input type="file" name="screenshot" id="screenshot"><br>
    <input type="submit" name="submit">
    </form>
<?php endif; ?>
