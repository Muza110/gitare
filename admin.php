<link rel="stylesheet" href="admin.css">
<?php
include "authenticate.php";
require_once "molla.php";

$dbc = mysqli_connect("localhost","root","","elvis_store");

$query = 'SELECT * FROM guitarwars order by score desc, date asc';

$data = mysqli_query($dbc,$query);

echo '<table>';
echo '<th>Name</th>'.'<th>Date</th>'.'<th>Score</th>'.'<th>Remove</th>'.'<th>Approve</th>';
while ($row = mysqli_fetch_array($data)){
    echo '<tr>'; // Open a new table row for each record
    echo '<td class="scorerow"><strong>'.$row['name'].'</strong></td>'; // Start with the first column
    echo '<td>'.$row['date'].'</td>';
    echo '<td>'.$row['score'].'</td>';
    echo '<td><a class="remove"; href="removescore.php?id=' . $row['id'] . '&amp;date=' . $row['date'] . '&amp;name=' . $row['name'] . '&amp;score=' . $row['score'] . '&amp;screenshot=' . $row['screenshot'] .'">Remove</a></td>'; // Action column for removal
    if ($row['approved']=='0'){
        echo '<td><a href="aprovescore.php?id=' . $row['id'] . '&amp;date=' . $row['date'] . '&amp;name=' . $row['name'] . '&amp;score=' . $row['score'] . '&amp;screenshot=' . $row['screenshot'] .'">Approve</a></td>'; // Action column for approval
    } elseif ($row['approved']=='1'){
        echo '<td>&#10004;</td>';
    }
    echo '</tr>'; // Close the table row
}
echo '</table>';
mysqli_close($dbc);
?>
