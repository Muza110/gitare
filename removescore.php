<link rel="stylesheet" href="style.css">
<h2>Guitar wars highscore reomve score</h2>
<?php
include "authenticate.php";
require_once 'molla.php';

if (isset($_GET['id']) && isset($_GET['date'])  && isset($_GET['name'])  && isset($_GET['score'])  && isset($_GET['screenshot'])) {
    $id = $_GET['id'];
    $name = $_GET['name'];
    $score = $_GET['score'];
    $date = $_GET['date']; // Corrected variable name
    $screenshot = $_GET['screenshot'];
} else if (isset($_POST['id']) && isset($_POST['name']) && isset($_POST['score'])) {
    
    $id = $_POST['id'];
    $name = $_POST['name'];
    $score = $_POST['score'];
} else {
    echo '<p class="error">Sorry, no high score was specified</p>'; // Corrected class attribute
}

if (isset($_POST['submit'])) {
    if ($_POST['confirm'] == 'Yes') { // Corrected field name
        // Delete the screenshot image from the server
        @unlink(PATH . $screenshot);

        $dbc = mysqli_connect("localhost", "root", "", "elvis_store");

        $query = "DELETE FROM guitarwars WHERE id = '$id'"; // Corrected SQL syntax
        mysqli_query($dbc, $query);
        mysqli_close($dbc);
        echo '<p>The high score of ' . $score . ' for ' . $name . ' was successfully removed</p>'; // Corrected spelling
    } else {
        echo '<p class="error">The high score was not removed you can chek it later</p>'; // Corrected class attribute
    }
} else if (isset($id) && isset($name) && isset($date) && isset($score) && isset($screenshot)) {
    echo '<p>Are you sure you want to delete the following high score?</p>';
    echo '<p><strong>Name: </strong>' . $name . '<br /><strong>Date: </strong>' . $date . '<br /><strong>Score: </strong>' . $score . '</p>';
    echo '<form method="post" action="removescore.php">';
    echo '<input type="radio" name="confirm" value="Yes" /> Yes ';
    echo '<input type="radio" name="confirm" value="No"/> No <br />';
    echo '<input type="submit" value="Submit" name="submit" />';
    echo '<input type="hidden" name="id" value="' . $id . '">';
    echo '<input type="hidden" name="name" value="' . $name . '">';
    echo '<input type="hidden" name="score" value="' . $score . '">';
    echo '</form>';
}
echo '<p><a href="admin.php">&lt;&lt; Back to admin page</a></p>';
?>
